﻿
namespace Battleship.Ascii
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using GameController;
    using GameController.Contracts;

    using System.Windows.Media;
    using System.Reflection;
    using System.Media;
    using System.Windows.Media;

    internal class Program
    {
        private static List<Ship> myFleet;

        private static List<Ship> enemyFleet;


        static void Main()
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.Clear();
            Console.WriteLine("                                     |__");
            Console.WriteLine(@"                                     |\/");
            Console.WriteLine("                                     ---");
            Console.WriteLine("                                     / | [");
            Console.WriteLine("                              !      | |||");
            Console.WriteLine("                            _/|     _/|-++'");
            Console.WriteLine("                        +  +--|    |--|--|_ |-");
            Console.WriteLine(@"                     { /|__|  |/\__|  |--- |||__/");
            Console.WriteLine(@"                    +---------------___[}-_===_.'____                 /\");
            Console.WriteLine(@"                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _");
            Console.WriteLine(@" __..._____--==/___]_|__|_____________________________[___\==--____,------' .7");
            Console.WriteLine(@"|                        Welcome to Battleship                         BB-61/");
            Console.ForegroundColor = ConsoleColor.DarkBlue;
            Console.WriteLine(@" uUuUuUUuuuUUUuUuuuuuuuuuuuuuuuuuuUUUUUuuUuUuUUUUUuuuuUUUuUUUUuuuuuuuuuuuuuU");
            Console.WriteLine(@" \________________________________________________________________________/");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Black;


            InitializeGame();

            StartGame();
        }

        private static void StartGame()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("                  __");
            Console.WriteLine(@"                 /  \");
            Console.WriteLine("           .-.  |    |");
            Console.WriteLine(@"   *    _.-'  \  \__/");
            Console.WriteLine(@"    \.-'       \");
            Console.WriteLine("   /          _/");
            Console.WriteLine(@"  |      _  /""");
            Console.WriteLine(@"  |     /_\'");
            Console.WriteLine(@"   \    \_/");
            Console.WriteLine(@"    """"""""");
            Console.ForegroundColor = ConsoleColor.Black;
            do
            {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine(@"------------------------");
                Console.WriteLine(@"-Player, it's your turn-");
                Console.WriteLine(@"------------------------");
                Console.WriteLine("Enter coordinates for your shot :");
                Console.ForegroundColor = ConsoleColor.Black;
                var position = ParsePosition(Console.ReadLine());
                var isHit = GameController.CheckIsHit(enemyFleet, position);
                if (isHit)
                {
                    Console.Beep();
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(@"                \         .  ./");
                    Console.WriteLine(@"              \      .:"";'.:..""   /");
                    Console.WriteLine(@"                  (M^^.^~~:.'"").");
                    Console.WriteLine(@"            -   (/  .    . . \ \)  -");
                    Console.WriteLine(@"               ((| :. ~ ^  :. .|))");
                    Console.WriteLine(@"            -   (\- |  \ /  |  /)  -");
                    Console.WriteLine(@"                 -\  \     /  /-");
                    Console.WriteLine(@"                   \  \   /  /");
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                else
                {
                    Console.Beep();
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(@"                \         .  ./");
                    Console.WriteLine(@"              \      .:"".""   /");
                    Console.WriteLine(@"            -   (/  .    . . \)  -");
                    Console.WriteLine(@"               ((|      |))");
                    Console.WriteLine(@"            -   (\- |    |  /)  -");
                    Console.WriteLine(@"                 -\  \     /  /-");
                    Console.WriteLine(@"                   \  \   /  /");
                    Console.ForegroundColor = ConsoleColor.Black;
                }

                Console.WriteLine(isHit ? "Yeah ! Nice hit !" : "Miss");

                do
                {
                    position = GetRandomPosition();
                } while (!GameController.IsPositionValid(position));
                GameController.PositionsEnemyShot.Add(position);
                isHit = GameController.CheckIsHit(myFleet, position);
                if (GameController.countHit == 5)
                {
                    break;
                }
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.WriteLine();
                Console.WriteLine(@"------------------------");
                Console.WriteLine(@"-    Machine's turn    -");
                Console.WriteLine(@"------------------------");
                Console.WriteLine("Computer shot in {0}{1} and {2}", position.Column, position.Row, isHit ? "has hit your ship !" : "miss");
                Console.ForegroundColor = ConsoleColor.Black;
                if (isHit)
                {
                    Console.Beep();
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(@"                \         .  ./");
                    Console.WriteLine(@"              \      .:"";'.:..""   /");
                    Console.WriteLine(@"                  (M^^.^~~:.'"").");
                    Console.WriteLine(@"            -   (/  .    . . \ \)  -");
                    Console.WriteLine(@"               ((| :. ~ ^  :. .|))");
                    Console.WriteLine(@"            -   (\- |  \ /  |  /)  -");
                    Console.WriteLine(@"                 -\  \     /  /-");
                    Console.WriteLine(@"                   \  \   /  /");
                    Console.ForegroundColor = ConsoleColor.Black;
                }
                else
                {
                    Console.Beep();
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(@"                \         .  ./");
                    Console.WriteLine(@"              \      .:"".""   /");
                    Console.WriteLine(@"            -   (/  .    . . \)  -");
                    Console.WriteLine(@"               ((|      |))");
                    Console.WriteLine(@"            -   (\- |    |  /)  -");
                    Console.WriteLine(@"                 -\  \     /  /-");
                    Console.WriteLine(@"                   \  \   /  /");
                    Console.ForegroundColor = ConsoleColor.Black;
                }
            }
            while (true);
            EndGame();
            FinalSurprise("SURPISE!!!!!!!!!!");
            Console.ReadLine();
            //var colors = GetAllColors();
            //foreach (Color color in colors) {
            //    /*Console.BackgroundColor =*/ color.;
            //    Console.
            //}
            //ConsoleColor.g
            //Console.WriteLine("SURPISE!!!!!!!!!!");
            //System.Threading.Thread.Sleep(2000);
            //Console.WriteLine("YOU WON!!!!!!!!!!");
        }

        private static List<Color> GetAllColors()
        {
            List<Color> allColors = new List<Color>();

            foreach (PropertyInfo property in typeof(Color).GetProperties())
            {
                if (property.PropertyType == typeof(Color))
                {
                    allColors.Add((Color)property.GetValue(null));
                }
            }

            return allColors;
        }

        private static void FinalSurprise(string message)
        {
            Type type = typeof(ConsoleColor);
            Console.ForegroundColor = ConsoleColor.White;
            foreach (var name in Enum.GetNames(type))
            {
                Console.BackgroundColor = (ConsoleColor)Enum.Parse(type, name);
                Console.WriteLine(message);
            }
            Console.BackgroundColor = ConsoleColor.Black;
            foreach (var name in Enum.GetNames(type))
            {
                Console.ForegroundColor = (ConsoleColor)Enum.Parse(type, name);
            }
        }

        internal static Position ParsePosition(string input)
        {
            //try {
            var letter = (Letters)Enum.Parse(typeof(Letters), input.ToUpper().Substring(0, 1));
            //} catch () {
            //    //Console.Write()
            //}

            var number = int.Parse(input.Substring(1, 1));
            return new Position(letter, number);
        }

        private static Position GetRandomPosition()
        {
            int rows = 8;
            int lines = 8;
            var random = new Random();
            var letter = (Letters)random.Next(lines);
            var number = random.Next(rows);
            var position = new Position(letter, number);
            return position;
        }

        private static void InitializeGame()
        {
            InitializeMyFleet();

            InitializeEnemyFleet();
        }

        private static void InitializeMyFleet()
        {
            myFleet = GameController.InitializeShips().ToList();

            Console.WriteLine("Please position your fleet (Game board size is from A to H and 1 to 8) :");
            Position aux;
            //String auxPosicion = "";
            foreach (var ship in myFleet)
            {
                Console.WriteLine();
                Console.WriteLine("Please enter the positions for the {0} (size: {1})", ship.Name, ship.Size);
                for (var i = 1; i <= ship.Size; i++)
                {
                    Console.WriteLine("Enter position {0} of {1} (i.e A3):", i, ship.Size);

                    aux = ParsePosition(Console.ReadLine());
                    if (ValidatePosition(aux))
                    {
                        ship.AddPosition(aux);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("WRONG POSITION, try another!");
                        Console.ForegroundColor = ConsoleColor.Black;
                        i--;
                    }

                }
            }
        }

        private static bool ValidatePosition(Position aux)
        {
            if (GameController.CheckIsHit(myFleet, aux))
            {
                return false;
            }
            return true;
        }

        private static void InitializeEnemyFleet()
        {
            enemyFleet = GameController.InitializeShips().ToList();

            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 4 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 5 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 6 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 7 });
            enemyFleet[0].Positions.Add(new Position { Column = Letters.B, Row = 8 });

            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 6 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 7 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 8 });
            enemyFleet[1].Positions.Add(new Position { Column = Letters.E, Row = 9 });

            enemyFleet[2].Positions.Add(new Position { Column = Letters.A, Row = 3 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.B, Row = 3 });
            enemyFleet[2].Positions.Add(new Position { Column = Letters.C, Row = 3 });

            enemyFleet[3].Positions.Add(new Position { Column = Letters.F, Row = 8 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.G, Row = 8 });
            enemyFleet[3].Positions.Add(new Position { Column = Letters.H, Row = 8 });

            enemyFleet[4].Positions.Add(new Position { Column = Letters.C, Row = 5 });
            enemyFleet[4].Positions.Add(new Position { Column = Letters.C, Row = 6 });
        }
        private static void EndGame()
        {
            SoundPlayer typewriter = new SoundPlayer();
            typewriter.SoundLocation = Environment.CurrentDirectory + "/endSound.wav";
            typewriter.PlayLooping();
            //typewriter.Play();

        }
    }
}
