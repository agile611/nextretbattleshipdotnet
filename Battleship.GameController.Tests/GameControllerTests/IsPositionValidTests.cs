﻿namespace Battleship.GameController.Tests.GameControllerTests
{
    using System;

    using Battleship.GameController.Contracts;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The game controller tests.
    /// </summary>
    [TestClass]
    public class IsPositionValidTests
    {
        /// <summary>
        /// The should hit the ship.
        /// </summary>
        [TestMethod]
        public void IsPositionValid1()
        {
            var position = new Position { Column= Letters.A, Row= 2}; 
            var result = GameController.IsPositionValid(position);

            Assert.IsTrue(result);
        }
        [TestMethod]
        public void IsPositionValid2()
        {
            var position = new Position { Column = Letters.A, Row = 1 };
            var result = GameController.IsPositionValid(position);

            Assert.IsTrue(result);
        }
       
        [TestMethod]
    public void IsPositionNotValid()
    {
            var position = new Position { Column = Letters.A, Row = 1 };

            var result = GameController.IsPositionValid(position);


        Assert.IsTrue(result);
    }
        //[TestMethod]
        //public void IsPositionNotValid2()
        //{
        //    var position = new Position { Column = Letters.A, Row = 9 };

        //    var result = GameController.IsPositionValid(position);


        //    Assert.IsFalse(result);
        //}
        //[TestMethod]
        //public void IsPositionNotValid3()
        //{
        //    var position = new Position { Column = Letters.A, Row = 1 };
        //    var result = GameController.IsPositionValid(position);

        //    Assert.IsFalse(result);
        //}
        ///// <summary>
        ///// The throw exception if positstion is null.
        ///// </summary>
        //[TestMethod]
        //[ExpectedException(typeof(ArgumentNullException))]
        //public void ThrowExceptionIfPositstionIsNull2()
        //{
        //    GameController.CheckIsHit(GameController.InitializeShips(), null);
        //}

       
    }
}